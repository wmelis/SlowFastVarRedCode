classdef LinearSFSystem < SlowFastSystem
% LinearSFSystem - Linear slow-fast system class
% Class that implements a linear stochastic slow-fast system of the form:
%   dx/dt = lambda*x + p*y
%   dy/dt = (1/epsi)*(q*x - A*y) + (1/sqrt(epsi))*dV/dt
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also SlowFastSystem, NonLinearSFSystemPK.
    properties
        lambda;
        p;
        q;
        A;
    end
    
    methods
        function process = LinearSFSystem(lambda,p,q,A,epsi)
            process = process@SlowFastSystem(epsi);
            
            process.lambda = lambda;
            process.p = p;
            process.q = q;
            process.A = A;
        end
    
        function func_f = f(self,x,y)
            func_f = self.lambda*x + self.p*y;
        end
        
        function func_alpha = alpha(self,x,y)
            func_alpha = 0;
        end
        
        function func_g = g(self,x,y)
            func_g = self.q*x - self.A*y;
        end
        
        function func_beta = beta(self,x,y)
            func_beta = 1;
        end
        
        function V = potential(self,x,y)
            V = self.A/2*y^2 - self.q*x*y;
        end
        
        function dVdx = dVdx(self,x,y)
            dVdx = -self.g(x,y);
        end
        
        function rho_inv = invariant_pdf_nonorm(self,x,y)
%             rho_inv = exp(-(y - self.q*x/self.A).^2/(1/self.A));
            rho_inv = exp(-2*self.potential(x,y));
        end
        
        % Exact macroscopic equation dX/dt = F(X)
        function func_F = F(self,x)
            func_F = (self.lambda + self.p*self.q/self.A)*x;
        end
        
        function x_e = exact_solution(self,x0,t)
            x_e = x0*exp((self.lambda + self.p*self.q/self.A)*t);
        end
        
        function mu_inv = get_invariant_mean(self,xcur,ycur)
            mu_inv = (self.q/self.A)*xcur;
        end
        
        function sig2_inv = get_invariant_variance(self,xcur,ycur)
            sig2_inv = (1/(2*self.A));
        end
    end
end