classdef SlowFastSystem < handle
% SlowFastSystem - Slow-fast system parent class
% Base class for defining stochastic slow-fast system of the form:
%   dx/dt = f(x,y) + alpha(x,y)dU/dt
%   dy/dt = (1/epsi)*g(x,y) + (1/sqrt(epsi))*beta(x,y)*dV/dt
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also LinearSFSystem, NonLinearSFSystemPK.
    properties
        epsi;
    end
    
    methods
        function system = SlowFastSystem(epsi)
            if (nargin == 1)           
                system.epsi = epsi;
            end
        end
       
        function drift = drift_slow(self,x,y)
            drift = self.f(x,y);
        end
        
        function diffusion = diff_slow(self,x,y)
            diffusion = self.alpha(x,y);
        end
        
        function drift = drift_fast(self,x,y)
            drift = (1/self.epsi)*self.g(x,y);
        end
        
        function diffusion = diff_fast(self,x,y)
            diffusion = sqrt(1/self.epsi)*self.beta(x,y);
        end
    end
   
    methods (Abstract)
        f(x,y); alpha(x,y);
        g(x,y); beta(x,y);
        potential(x,y); dVdx(x,y);
        invariant_pdf_nonorm(x,y);
    end        
end