classdef NonLinearSFSystemPK < SlowFastSystem
% NonLinearSFSystemPK - Nonlinear slow-fast system class
% Class that implements a nonlinear stochastic slow-fast system of the form:
%   dx/dt = -(y + y^2)
%   dy/dt = (1/epsi)*(x - y) + (1/sqrt(epsi))*dV/dt
% from the paper by Papavasiliou and Kevrekidis, MMS 6(1), 2007.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also SlowFastSystem, LinearSFSystem.    
    methods
        function system = NonLinearSFSystemPK(epsi)
            system = system@SlowFastSystem(epsi);
        end
    
        function func_f = f(self,x,y)
            func_f = -(y + y.^2);
        end
        
        function func_alpha = alpha(self,x,y)
            func_alpha = 0;
        end
        
        function func_g = g(self,x,y)
            func_g = (x - y);
        end
        
        function func_beta = beta(self,x,y)
            func_beta = 1;
        end
        
        function V = potential(self,x,y)
            V = y^2/2 - x*y;
        end
        
        function dVdx = dVdx(self,x,y)
            dVdx = -self.g(x,y);
        end
        
        function rho_inv = invariant_pdf_nonorm(self,x,y)
            rho_inv = exp(-2*self.potential(x,y));
        end
        
        % Exact macroscopic equation dX/dt = F(X)
        function func_F = F(self,x)
            func_F = -(x.^2 + x + 1/2);
        end
        
        function x_e = exact_solution(self,x0,t)
            x_e = -1/2*(1 + tan(t/2 - atan(2*x0+1)));
%             x_e = -1/2 - sqrt(3)/2*tan(sqrt(3)/2*(t - atan(2*x0+1)));
        end
        
        function mu_inv = get_invariant_mean(self,xcur,ycur)
            mu_inv = xcur;
        end
        
        function sig2_inv = get_invariant_variance(self,xcur,ycur)
            sig2_inv = 1/2;
        end
    end
end