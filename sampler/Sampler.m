classdef Sampler < handle
% Sampler - Sampler parent class
% Base class for defining sampling methods that generate samples from the invariant
% measure induced by the fast dynamics of a stochastic slow-fast system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also ExactSampler, MarkovChainSampler, MetropolisSampler.
    properties
        Mb = 0;  % burn-in (number of samples to discard)
    end
    
    methods (Abstract)
        get_samples(self,x0,y0,xi);
    end
    
    methods (Static)
        function sampler = getSampler(type,method,Mb)
            switch type
                case 'markov'
                    sampler = MarkovChainSampler(method,Mb);
                case 'exact'
                    system = method.system;
                    sampler = ExactSampler(system);
                case 'metropolis'
                    sampler = MetropolisSampler(method);
                otherwise
                    error(['Unknown sampling method: ' type]);
            end
        end
    end
end