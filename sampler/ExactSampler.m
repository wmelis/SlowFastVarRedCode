classdef ExactSampler < Sampler
% ExactSampler - Exact sampler class
% Class that implements an exact sampling method, which generate samples from the exact
% invariant measure induced by the fast dynamics of a stochastic slow-fast system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also Sampler, MarkovChainSampler, MetropolisSampler.
    properties
        system;
    end
    
    methods
        function sampler = ExactSampler(system)
            sampler.system = system;
        end
        
        function y = get_samples(self,x0,y0,xi)
            mu_inv = self.system.get_invariant_mean(x0,y0);
            s2_inv = self.system.get_invariant_variance(x0,y0);
            
            y = mu_inv + sqrt(s2_inv)*xi;
        end
    end
end