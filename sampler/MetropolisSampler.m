classdef MetropolisSampler < Sampler
% MetropolisSampler - Metropolis sampler class
% Class that implements a Metropolis sampling method, which adds an accept-reject step to
% the Markov chain sampler that corrects for the difference between the Markov chain
% generated invariant measure and the true invariant measure of the fast dynamics of the 
% stochastic slow-fast system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also Sampler, ExactSampler, MarkovChainSampler.
    properties
        integrator;
        n_accept = 0;
        n_reject = 0;
    end
    
    methods
        function sampler = MetropolisSampler(integrator)
            sampler.integrator = integrator;
        end
        
        function ytime = get_samples(self,x0,y0,xi)
            M = length(xi);
            U = rand(1,M-1);
            ytime = zeros(1,M);
            ytime(1) = y0;
            y = y0;
            m = 1;
            while (m < M)
                ytrial = self.integrator.step_fast(x0,y,xi(m));
                
                rho_ratio = self.integrator.system.invariant_pdf_nonorm(x0,ytrial)/self.integrator.system.invariant_pdf_nonorm(x0,y);
                T_ratio = self.integrator.transition_density(x0,ytrial,y)/self.integrator.transition_density(x0,y,ytrial);
                rate = min(1,rho_ratio*T_ratio);
                if (U(m) <= rate)
                    y = ytrial;
                    self.n_accept = self.n_accept + 1;
                else
                    self.n_reject = self.n_reject + 1;
                end
                ytime(m+1) = y;
                
                m = m + 1;
            end
        end
    end
end