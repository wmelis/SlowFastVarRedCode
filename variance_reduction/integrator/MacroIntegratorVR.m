classdef MacroIntegratorVR < MacroIntegrator
% MacroIntegratorVR - Macroscopic variance-reduced time integrator class
% Class that implements a macroscopic variance-reduced time integrator for simulation of 
% the averaged equation dX/dt = F(X) of a stochastic slow-fast system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also MacroIntegrator.
    properties
        initializer;
        rest_exact;
        reinitializer;
        reinit_steps;
        M_r; % number of samples to calculate reinitialized F
    end
    
    methods
        function macro = MacroIntegratorVR(Dt,M,samp,est,initializer,r_e,reinit_steps,reinitializer,M_r)
            macro = macro@MacroIntegrator(Dt,M,samp,est);
            
            macro.initializer = initializer;
            macro.rest_exact = r_e;
            
            macro.reinit_steps = reinit_steps;
            macro.reinitializer = reinitializer;
            macro.M_r = M_r;
        end

        function [xtime,Fhat_prev,Fhat_cur,Fbar_cur] = integrate(self,treport,x0,y0)
            rest_exact = self.rest_exact;
            reinit_steps = self.reinit_steps;
            acc = 10^-14;
            nreport = length(treport);
            xtime = zeros(1,nreport);
            Fbar_cur = zeros(1,nreport-1);
            Fhat_cur = zeros(1,nreport-2);
            Fhat_prev = zeros(1,nreport-2);
            
            % Initial condition: X^0 = x_0
            xtime(1) = x0;
            
            % First step: integrate with exact or estimated solution for F
            [y01,Fbar_cur(1)] = self.initializer.get_init_F(x0,y0);
            xtime(2) = x0 + self.dt*Fbar_cur(1);
            
            % Other steps: loop over them
            i_reinit = 1;
            nxi = self.M + self.sampler.Mb - 1;
            self.t = treport(2);
            ireport = 2;
            while (abs(self.t - treport(end)) >= 100*eps)
                if (ireport == reinit_steps(i_reinit))
%                     reinitializing = 0;
%                     Fbar_prev = self.estimator.system.F(Xprev);
                    xi = randn(1,self.M_r);
                    y0 = y01(end);
                    y03 = self.reinitializer.get_samples(xtime(ireport),y0,xi);
                    Fbar_cur(ireport) = self.estimator.F_hat(xtime(ireport),y03);
                    i_reinit = i_reinit + 1;
                else
                    xi = randn(1,nxi);

                    % Calculate Fhat^n with seed omega_n
                    Xcur = xtime(ireport);
                    y0 = y01(end);
                    y01 = self.sampler.get_samples(Xcur,y0,xi);
                    Fhat_cur(ireport-1) = self.estimator.F_hat(Xcur,y01);

                    % Calculate Fhat^(n-1) with seed omega_n
                    Xprev = xtime(ireport-1);
                    y02 = self.sampler.get_samples(Xprev,y0,xi);
                    Fhat_prev(ireport-1) = self.estimator.F_hat(Xprev,y02);

                    % Calculate Fbar^(n-1)
                    if (rest_exact)
                        Fbar_prev = self.estimator.system.F(Xprev);
                    else
                        Fbar_prev = Fbar_cur(ireport-1);
                    end
                    
                    % Apply control variables variance reduction technique
                    Fbar_cur(ireport) = Fhat_cur(ireport-1) - Fhat_prev(ireport-1) + Fbar_prev;
                end
                
                xtime(ireport+1) = xtime(ireport) + self.dt*Fbar_cur(ireport);
                
                ireport = ireport + 1;

                self.t = self.t + self.dt;
                self.t = round(self.t/acc)*acc;
            end
        end
    end
end
