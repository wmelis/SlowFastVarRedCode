classdef InitializerVR < handle
% InitializerVR - Initializer parent class
% Base class for defining initializers to perform the first estimation in the
% variance-reduced HMM estimator.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also ExactInitializerVR, AverageHMMInitializerVR, MstarInitializerVR.
    methods (Abstract)
        get_init_F(self,x0,y0);
    end
end