classdef MstarInitializerVR < InitializerVR
% MstarInitializerVR - More accurate HMM initializer class
% Class that implements the initialization of the variance-reduced HMM estimator as a more
% accurate HMM estimator using more samples denoted by Mstar >> M.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also InitializerVR, ExactInitializerVR, AverageHMMInitializerVR.
    properties
        estimator;
        sampler;
        M_star;
    end
    
    methods
        function initializer = MstarInitializerVR(estimator,sampler,M_star)
            initializer.estimator = estimator;
            initializer.sampler = sampler;
            initializer.M_star = M_star;
        end
        
        function [y01,F0] = get_init_F(self,x0,y0)
            nxi = self.M_star + self.sampler.Mb - 1;
            xi = randn(1,nxi);
            y01 = self.sampler.get_samples(x0,y0,xi);

            F0 = self.estimator.F_hat(x0,y01);
            y01 = y01(end);
        end
    end
end