classdef AverageHMMInitializerVR < InitializerVR
% AverageHMMInitializerVR - Averaged HMM initializer class
% Class that implements the initialization of the variance-reduced HMM estimator as the
% average of S HMM estimators.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also InitializerVR, ExactInitializerVR, MstarInitializerVR.
    properties
        estimator;
        sampler;
        M;
        S;
    end
    
    methods
        function initializer = AverageHMMInitializerVR(estimator,sampler,M,S)
            initializer.estimator = estimator;
            initializer.sampler = sampler;
            initializer.M = M;
            initializer.S = S;
        end
        
        function [y01,F0] = get_init_F(self,x0,y0)
            nxi = self.M + self.sampler.Mb - 1;
            xi = randn(self.S,nxi);
            y01 = self.sampler.get_samples(x0,y0,xi);

            F0 = mean(self.estimator.F_hat(x0,y01),1);
            y01 = mean(y01(:,end),1);
        end
    end
end