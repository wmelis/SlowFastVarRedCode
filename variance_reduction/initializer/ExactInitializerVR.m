classdef ExactInitializerVR < InitializerVR
% ExactInitializerVR - Exact initializer class
% Class that implements the initialization of the variance-reduced HMM estimator as the
% exact value of the initial time derivative.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also InitializerVR, AverageHMMInitializerVR, MstarInitializerVR.
    properties
        system;
    end
    
    methods
        function initializer = ExactInitializerVR(system)
            initializer.system = system;
        end
        
        function [y01,F0] = get_init_F(self,x0,y0)
            y01 = y0;
            F0 = self.system.F(x0);
        end
    end
end