% Configuration file of the stochastic slow-fast systems under study.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
switch slowfast_system
    case 'linear'
        x0 = 1;
        y0 = 1;

        lambda = -10;
        p = 4;
        q = 0.5;
        A = 1.2;
        if ((lambda + p*q/A) >= 0)
            error('Exact solution is increasing!');
        end
        system = LinearSFSystem(lambda,p,q,A,epsi);
        
    case 'nonlinPK'
        x0 = 0.5;
        y0 = 0.5;
        system = NonLinearSFSystemPK(epsi);

    otherwise
        error(['Unknown slow/fast system provided: ' slowfast_system]);
end
