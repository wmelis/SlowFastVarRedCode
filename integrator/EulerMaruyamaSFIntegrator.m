classdef EulerMaruyamaSFIntegrator < StochasticSFIntegrator
% EulerMaruyamaSFIntegrator - Euler-Maruyama integrator class
% Class that implements the Euler-Maruyama time integrator for stochastic slow-fast 
% systems.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also StochasticSFIntegrator.
    methods
        function FE = EulerMaruyamaSFIntegrator(varargin)
            FE = FE@StochasticSFIntegrator(varargin{:});
        end
        
        function x = step_slow(self,x0,y0,xi)
            x = x0 + self.dt*self.system.drift_slow(x0,y0) + sqrt(self.dt)*self.system.diff_slow(x0,y0).*xi;
        end
        
        function y = step_fast(self,x0,y0,xi)
            y = y0 + self.dt*self.system.drift_fast(x0,y0) + sqrt(self.dt)*self.system.diff_fast(x0,y0).*xi;
        end
        
        function T = transition_density(self,x,y,y2)
            a = self.dt/self.system.epsi;
            mu = y - a*self.system.dVdx(x,y);
            sig2 = a*(self.system.beta(x,y))^2;
            
            T = 1/sqrt(2*pi*sig2)*exp(-(y2 - mu)^2/(2*sig2));
        end
    end
end