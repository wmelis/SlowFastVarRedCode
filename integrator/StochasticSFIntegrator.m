classdef StochasticSFIntegrator < handle
% StochasticSFIntegrator - Stochastic slow-fast integrator parent class
% Base class for defining time integration methods for stochastic slow-fast systems.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also EulerMaruyamaSFIntegrator.
    properties
        dt;
        system;
        t = 0;
    end
    
    methods
        function integrator = StochasticSFIntegrator(dt,system)
            if (nargin == 2)
                integrator.dt = dt;
                integrator.system = system;
            end
        end
        
        function ztime = integrate(self,treport,x0,y0)
            acc = 10^-12;
            self.t = treport(1);
            nreport = length(treport);
            npaths = length(x0);
            xnew = x0;
            ynew = y0;
            ztime = zeros(nreport,2*npaths);
            ztime(1,:) = [x0 y0];
            ireport = 2;
            while (self.t < treport(end))
                x = xnew;
                y = ynew;
                z = [x; y];
                xnew = self.step_slow(x,y,randn(1,npaths));
                ynew = self.step_fast(x,y,randn(1,npaths));
                znew = [xnew; ynew];
                
                self.t = self.t + self.dt;
                self.t = round(self.t/acc)*acc;
                if (ireport <= nreport)
                    if (abs(self.t - treport(ireport)) <= 1000*eps)
                        alpha = (self.t - treport(ireport))/self.dt;
                        z2 = alpha*z + (1-alpha)*znew;
                        ztime(ireport,:,:) = z2;
                        ireport = ireport + 1;
                    end
                end
            end
        end
    end
    
    methods (Abstract)
        step_slow(x0,y0);
        step_fast(x0,y0);
    end
    
    methods (Static)
        function int = getIntegrator(name,dt,system)
            switch name
                case 'fe'
                    int = EulerMaruyamaSFIntegrator(dt,system);
                otherwise
                    error(['Unknown direct integrator method: ' name]);
            end
        end
    end
end