classdef MacroIntegrator < handle
% MacroIntegrator - Macroscopic time integrator class
% Class that implements a macroscopic time integrator for simulation of the averaged 
% equation dX/dt = F(X) of a stochastic slow-fast system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also MacroIntegratorVR.
    properties
        dt; % Macroscopic time step
        M;  % Number of samples
        sampler;   % Sampling method to sample invariant distribution
        estimator; % Macroscopic estimator method for dX/dt = F(X)
        t = 0;
    end
    
    methods
        function macro = MacroIntegrator(dt,M,sampler,estimator)
            if (nargin == 4)
                macro.dt = dt;
                macro.M = M;
                macro.sampler = sampler;
                macro.estimator = estimator;
            end
        end
        
        function [xtime,Ftime] = integrate(self,treport,x0,y0)
            acc = 10^-14;
            nreport = length(treport);
            xtime = zeros(1,nreport);
            Ftime = zeros(1,nreport-1);
            
            % Initial condition: X^0 = x_0
            xtime(1) = x0;
            
            % Other steps: loop over them
            self.t = treport(1);
            ireport = 1;
            while (abs(self.t - treport(end)) >= 100*eps)
                xi = randn(1,self.M);
                % Generate samples from the invariant distribution of y for fixed x.
                y0 = self.sampler.get_samples(x0,y0,xi);
                
                Ftime(ireport) = self.estimator.F_hat(x0,y0);
                xtime(ireport+1) = xtime(ireport) + self.dt*Ftime(ireport);
                
                ireport = ireport + 1;
                x0 = xtime(ireport);
                y0 = y0(end); % last y-value of previous iteration

                self.t = self.t + self.dt;
                self.t = round(self.t/acc)*acc;
            end
        end
    end
end