classdef MacroEstimator < handle
% MacroEstimator - Macroscopic estimator parent class
% Base class for defining estimators that approximate the time derivative of the averaged
% equation of a stochastic slow-fast system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also MacroEstimatorHMM.
    properties
        system;
    end
    
    methods
        function estimator = MacroEstimator(system)
            if (nargin == 1)
                estimator.system = system;
            end
        end 
    end
    
    methods (Abstract)
        F = F_hat(self,x0,y0);
    end
end