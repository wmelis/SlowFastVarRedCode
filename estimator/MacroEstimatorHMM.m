classdef MacroEstimatorHMM < MacroEstimator
% MacroEstimatorHMM - HMM estimator class
% Class that implements the HMM (Heterogeneous Multiscale Method) estimator for the time
% derivative of the averaged equation of a stochastic slow-fast system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%
% See also MacroEstimator.    
    methods
        function estimator = MacroEstimatorHMM(system)
            estimator = estimator@MacroEstimator(system);
        end
        
        function F_HMM = F_hat(self,x0,y0)
            F_HMM = mean(self.system.drift_slow(x0,y0),2);
        end
        
        function alpha_HMM = alpha_hat(self,x0,y0)
            alpha_HMM = mean(self.system.diff_slow(x0,y0));
        end
    end
end