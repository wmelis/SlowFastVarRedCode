% Configuration file for the simulation of the averaged equation of a stochastic slow-fast 
% system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016
%% Parameter values
epsi = 10^(-3);  % relaxation (scaling) parameter
M = 0.05*10^3;   % Number of samples from invariant distribution
Dt = 0.05;       % Macroscopic time step

%% Slow/fast system
config_system;

%% Sampler configuration
dt = epsi;   % stochastic integrator time step
if (strcmp(slowfast_system,'linear') && variance_reduction)
    dt = 2*epsi/A; % time step choice in case of the linear system to avoid a bias
end
Mb = 0;  % burn-in (number of samples to discard; only for MarkovChainSampler)
sampling_method = StochasticSFIntegrator.getIntegrator('fe',dt,system);
sampler = Sampler.getSampler(sampler_type,sampling_method,Mb);

%% Macro estimator configuration
switch macro_estimator_type
    case 'hmm'
        macro_estimator = MacroEstimatorHMM(system);
        
    otherwise
        error(['Unknown integration type selected: ' integrator_type]);
end

%% Macro integrator configuration
disp([' ------------- ' mfilename ' (' datestr(now) ') --------------']);
disp(['  * Slow/fast system: ' slowfast_system ' (epsi = ' num2str(epsi) ')']);

switch variance_reduction
    case 0
        integrator = MacroIntegrator(Dt,M,sampler,macro_estimator);
        disp(['  * Estimator: ' upper(macro_estimator_type) ' (Dt = ' num2str(Dt) ')']);
        disp(['  * Sampler: ' sampler_type ' (M = ' num2str(M) '; Mb = ' num2str(Mb) ')']);
    case 1
        disp(['  * Estimator: VR ' upper(macro_estimator_type) ' (Dt = ' num2str(Dt) ')']);
        disp(['  * Sampler: ' sampler_type ' (M = ' num2str(M) '; Mb = ' num2str(Mb) ')']);
        
        first_step = 'hmm_high_M'; % exact  hmm_high_M  hmm_average
        
        switch first_step
            case 'exact'
                initializer = ExactInitializerVR(system);
                disp('    * Initialization: exact');
            case 'hmm_high_M'
                M_star = 5*10^2;
                sampler_init_type = 'metropolis'; % markov  exact  metropolis
%                 sampling_method_init = StochasticSFIntegrator.getIntegrator('fe',epsi,system);
                sampler_init = Sampler.getSampler(sampler_init_type,sampling_method,Mb);
                initializer = MstarInitializerVR(macro_estimator,sampler_init,M_star);
                disp(['    * Initialization: estimated (' sampler_init_type '; M^* = ' num2str(M_star) ')']);
            case 'hmm_average'
                S = 10;
                sampler_init_type = 'markov'; % markov  exact  metropolis
%                 sampling_method_init = StochasticSFIntegrator.getIntegrator('fe',epsi,system);
                sampler_init = Sampler.getSampler(sampler_init_type,sampling_method,Mb);
                initializer = AverageHMMInitializerVR(macro_estimator,sampler_init,M,S);
                disp(['    * Initialization: averaged (' sampler_init_type '; S = ' num2str(S) ')']);
        end
        
        rest_exact = 0;
        reinit = 0;
        R = 2; % number of macroscopic time steps after which to reinitialize
        % Calculate macroscopic steps on which to reinitialize. The last term avoids using
        % the boolean 'reinit' in the VR HMM class.
        reinit_steps = [(1:((2/Dt)/R - 1))*R 2000] + (1-reinit)*10^8;
        M_r = 5*10^2;
        sampler_reinit_type = 'metropolis'; % markov  exact  metropolis
        sampler_reinit = Sampler.getSampler(sampler_reinit_type,sampling_method,Mb);
        
        integrator = MacroIntegratorVR(Dt,M,sampler,macro_estimator,initializer,rest_exact,reinit_steps,sampler_reinit,M_r);
        
        if (reinit)
            disp(['    * Reinitialization: ' sampler_reinit_type ', M_r = ' num2str(M_r) '. Reinitializing after steps: ' num2str(reinit_steps) '.']);
        end
end
