% Main file for the simulation of the averaged equation of a stochastic slow-fast system.
%
%  Author: Ward Melis (ward.melis@gmail.com)
%  Date:   25 October 2016

% rng default; % uncomment to generate same random numbers

clc;
close all;
clear all;

%% Main configuration parameters
slowfast_system = 'linear'; % linear  nonlinPK
macro_estimator_type = 'hmm'; % hmm
sampler_type = 'markov';  % markov  exact  metropolis
variance_reduction = 0;   % 0: off; 1: on
exact_solution_known = 1; % 0: unknown; 1: known

config;

%% Numerical simulation
tend = 2;
t = 0:Dt:tend;

if (variance_reduction == 0)
    [x,F] = integrator.integrate(t,x0,y0);
else
    [x,F_est_prev,F_est_cur,F] = integrator.integrate(t,x0,y0);
end

if (exact_solution_known)
    x_e = system.exact_solution(x0,t);
    F_e = system.F(x_e(1:end-1));
end

x_fe = zeros(1,length(t));
F_fe = zeros(1,length(t)-1);
x_fe(1) = x0;
for i=1:length(t)-1
    F_fe(i) = system.F(x_fe(i));
    x_fe(i+1) = x_fe(i) + Dt*F_fe(i);
end

%% Results
fig_id = ceil(1000*rand);
figure(fig_id);
if (exact_solution_known)
    subplot(221); hold on; plot(t,x,'b-'); plot(t,x_e,'r','LineWidth',1.0); title('Simulation (path)');
    xlim([0 tend]); xlabel('time'); ylabel('X(t)'); legend('Simulation','Exact');

    subplot(222); hold on; plot(t,abs(x-x_e),'r-'); plot(t,abs(x-x_fe),'b-'); title('Error of estimated path');
    xlim([0 tend]); xL = xlim; line(xL,[0 0],'Color','k','LineStyle','--');
    legend('Ex X','FE X');

    subplot(223); hold on; plot(t(2:end),F,'b-'); plot(t(2:end),F_e,'r','LineWidth',1.0);
    plot(t(2:end),F_fe,'g-'); plot(t(2:end),system.F(x(1:end-1)),'m-'); title('Simulation (force)');
    xlim([0 tend]); xlabel('time'); ylabel('F(X)');
    legend('Est F in Est X','Ex F in Ex X','Ex F in FE X','Ex F in Est X');

    subplot(224); hold on; plot(t(2:end),abs(F-F_e),'r-'); plot(t(2:end),abs(F-F_fe),'g-');
    plot(t(2:end),abs(F-system.F(x(1:end-1))),'m-'); title('Error of estimator'); 
    xlim([0 tend]); xL = xlim; line(xL,[0 0],'Color','k','LineStyle','--');
    legend('Ex F in Ex X','Ex F in FE X','Ex F in Est X');
else
    subplot(121); hold on; plot(t,x,'b-'); plot(t,x_fe,'r','LineWidth',1.0); title('Simulation (path)');
    xlim([0 tend]); xlabel('time'); ylabel('X(t)'); legend('Simulation','FE');
    
    subplot(122); hold on; plot(t(2:end),F,'b-'); plot(t(2:end),F_fe,'r','LineWidth',1.0);
    xlim([0 tend]); xlabel('time'); ylabel('F(X)'); title('Simulation (force)');
end
